var game = {
  processing: null,
  stage: 0,
  centerX: 0,
  centerY: 0,
  width: 0,
  height: 0,
  dot: {
    pointX: 0,
    pointY: 0,
    directionX: 1,
    directionY: 1,
    speed: 5,
    size: 10,
    color: 0
  }
};

function draw () {
  drawBackground();
  moveBall();
  drawBall();
}

function setup () {
  game.width = window.innerHeight;
  game.height = window.innerHeight;

  game.processing.size(game.width, game.height);

  game.centerX = game.width / 2;
  game.centerY = game.height / 2;

  game.dot.pointX = game.centerX/3;
  game.dot.pointY = game.centerY/14;
}

function moveBall () {
  var dot = game.dot;

  if (dot.pointY < 0 || dot.pointY >= game.height) {
    dot.directionY *= -1;
  }

  if (dot.pointX < 0 || dot.pointX >= game.width) {
    dot.directionX *= -1;
  }

  dot.pointX += (dot.speed * dot.directionX);
  dot.pointY += (dot.speed * dot.directionY);
}

function drawBackground () {
  game.processing.background(224);
}

function drawBall () {
  var dot = game.dot;

  game.processing.ellipse(dot.pointX, dot.pointY, dot.size, dot.size);
  game.processing.fill(dot.color);
}


function initialize(processing) {
  game.processing = processing;

  processing.draw = draw;
  processing.setup = setup;
}


window.onload = function() {
  var canvas = document.getElementById("game_canvas");
  var container = document.getElementById("canvas_container");
  canvas.setAttribute('tabindex','0');
  canvas.focus();

  container.setAttribute("style","width:" + window.innerHeight + "px");
  new Processing(canvas, initialize);
};

