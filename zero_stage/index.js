var game = {
  processing: null,
  centerX: 0,
  centerY: 0,
  height: 0,
  count: 0,
  width: 0
};

function draw () {
  game.processing.background(222, 222, 222);
  game.count++;

  game.processing.textSize(28);

  game.processing.textAlign(game.processing.PConstants.CENTER,
    game.processing.PConstants.CENTER);

  game.processing.text("Hello World!", game.centerX, game.centerY);
  game.processing.text(game.count, game.centerX, game.centerY + 100);

  game.processing.fill(0, 0, 0);
}

function setup() {
  game.height = game.width = window.innerHeight;

  game.centerX = game.width / 2;
  game.centerY = game.height / 2;

  game.processing.size(game.width, game.height);
}

function initialize(processing) {
  game.processing = processing;

  processing.setup = setup;
  processing.draw = draw;
}

window.onload = function() {
  var container = document.getElementById("canvas_container");
  var canvas = document.getElementById("game_canvas");

  container.setAttribute("style", "width:" + window.innerHeight + "px");

  canvas.setAttribute('tabindex','0');
  canvas.focus();

  new Processing(canvas, initialize);
};