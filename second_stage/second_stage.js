var game = {
  processing: null,
  centerX: 0,
  centerY: 0,
  width: 0,
  height: 0,
  enemyCount: 20,
  enemies: [],
  enemy: {
    width: 0,
    height: 0,
    color: [0]
  },
  dot: {
    pointX: 0,
    pointY: 0,
    directionX: 1,
    directionY: 1,
    speed: 5,
    size: 10,
    color: 0
  },
  player: {
    pointX: 0,
    pointY: 0,
    width: 200,
    height: 10,
    color: 0
  }
};

function draw () {
  drawBackground();
  moveBall();

  drawBall();
  drawEnemies();
  drawPlayer();
}

function keyPressed () {
  if (game.processing.keyCode === 39) {
    movePlayer(false);
  }

  if (game.processing.keyCode === 37) {
    movePlayer(true);
  }
}

function setup() {
  game.height = game.width = window.innerHeight;

  game.processing.size(game.width, game.height);

  game.centerX = game.width / 2;
  game.centerY = game.height / 2;

  startTheGame();
}

function startTheGame () {
  game.stage = -2;

  game.enemyCount = 29;

  game.dot.speedX = Math.random() * (5 - 8) + 5;
  game.dot.speedY = Math.random() * (5 - 8) + 5;

  game.dot.pointX = 0;
  game.dot.pointY = game.centerY;

  game.player.pointX = game.centerX - (game.player.width/2);
  game.player.pointY = game.height - (game.player.height);

  game.enemy.width = game.width/19;
  game.enemy.height = game.width/19;

  initializeEnemies();
}

function initializeEnemies () {
  var i = 0,
    offsetX = 0,
    offsetY = 0;

  game.enemies = [];

  for (i; i < game.enemyCount; i++) {
    if (offsetX >= game.width) {
      offsetY += game.enemy.height;
      offsetX = (parseInt(offsetX, 10) === game.width) ? 0 : game.enemy.width;
    }

    game.enemies.push({
      pointX: offsetX,
      pointY: offsetY
    });

    offsetX += game.enemy.width * 2;
  }
}

function moveBall () {
  var dot = game.dot;

  if ((dot.pointX > game.player.pointX && dot.pointX < game.player.pointX + game.player.width)
    && dot.pointY >= game.height - game.player.height) {
    dot.directionY *= -1;
  }

  if (dot.pointY >= game.height) {
    dot.pointX = 0;
    dot.pointY = 0;
  }

  if (dot.pointY < 0) {
    dot.directionY *= -1;
  }

  if (dot.pointX < 0 || dot.pointX >= game.width) {
    dot.directionX *= -1;
  }

  dot.pointX += (dot.speed * dot.directionX);
  dot.pointY += (dot.speed * dot.directionY);
}

function movePlayer (direction) {
  var offset = direction ? - 50 : 50;

  if (game.player.pointX + offset > -game.player.width/2 && game.player.pointX + offset < (game.width - game.player.width/2)) {
    game.player.pointX += offset;
  }
}
function drawBackground () {
  game.processing.background(224);
}

function drawBall () {
  var dot = game.dot;

  game.processing.ellipse(dot.pointX, dot.pointY, dot.size, dot.size);
  game.processing.fill(dot.color);
}

function drawPlayer () {
  var player = game.player;

  game.processing.rect(player.pointX, player.pointY, player.width, player.height);
  game.processing.fill(player.color);
}

function drawEnemies () {
  var enemy = game.enemy;
  var i = 0;

  for (i; i < game.enemyCount; i++) {
    game.processing.fill.apply(game.processing, enemy.color);

    game.processing.rect(game.enemies[i].pointX, game.enemies[i].pointY,
      enemy.width, enemy.height);
  }
}

function initialize(processing) {
  game.processing = processing;

  processing.keyPressed = keyPressed;
  processing.setup = setup;
  processing.draw = draw;
}


window.onload = function() {
  var canvas = document.getElementById("game_canvas");
  var container = document.getElementById("canvas_container");
  canvas.setAttribute('tabindex','0');
  canvas.focus();

  container.setAttribute("style","width:" + window.innerHeight + "px");
  new Processing(canvas, initialize);
};

