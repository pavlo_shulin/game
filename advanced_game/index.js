var game = {
  processing: null,
  ready: false,
  stage: 1,
  centerX: 0,
  centerY: 0,
  width: 0,
  height: 0,
  enemyCount: 0,
  enemyKilled: 0,
  background: null,
  enemy: {
    width: 0,
    height: 0,
    color: [0],
    image: null
  },
  enemies: [

  ],
  text: {
    fontSize: 40,
    boxHeight: 250,
    color: [255, 255, 100]
  },
  dot: {
    pointX: 0,
    pointY: 0,
    directionX: -1,
    directionY: -1,
    speedX: 5,
    speedY: 5,
    size: 15,
    color: [200]
  },
  player: {
    pointX: 0,
    pointY: 0,
    width: 150,
    height: 30,
    image: null
  }
};

function draw () {
  if (!game.ready) return;

  if (game.stage === 1) {
    drawBackground();
    moveBall();
    checkPlayerWin();
    drawBall();
    drawEnemies();
    drawPlayer();
  } else if (game.stage === 0) {
    drawPause();
  } else if (game.stage === -1) {
    drawBackground();
    drawGameOver();
  } else if (game.stage === 2) {
    drawBackground();
    drawYouWin();
  } else if (game.stage === - 2) {
    drawBackground();
    drawStartTheGame();
  }
}

 function keyPressed () {
  if (game.stage === -2) {
    game.stage = 1;
  } else if (game.stage === -1){

  } else if (game.stage === 0) {
    if (game.processing.keyCode === 32) {
      game.stage = 1;
    }
  } else if (game.stage === 1) {
    if (game.processing.keyCode === 32) {
      game.stage = 0;
    }

    if (game.processing.keyCode === 39) {
      movePlayer(false);
    }

    if (game.processing.keyCode === 37) {
      movePlayer(true);
    }
  }
}

function setup() {
  game.height = game.width = window.innerHeight;

  game.processing.size(game.width, game.height);

  game.centerX = game.width / 2;
  game.centerY = game.height / 2;

  startTheGame();
}

function startTheGame () {
  game.stage = -2;

  game.enemyCount = 29;
  game.enemyKilled = 0;

  game.dot.speedX = Math.random() * (5 - 8) + 5;
  game.dot.speedY = Math.random() * (5 - 8) + 5;

  game.dot.pointX = 0;
  game.dot.pointY = game.centerY;

  game.player.pointX = game.centerX - (game.player.width/2);
  game.player.pointY = game.height - (game.player.height);

  game.enemy.width = game.width/19;
  game.enemy.height = game.width/19;

  loadData();
  initializeEnemies();
}

function loadData () {
  var imageCount = 0;

  var load = function () {
    imageCount++;

    if (imageCount === 3) {
      game.ready = true;
    }
  };

  // Magic
  var enemy = game.processing.loadImage("../resources/enemy.png", null, function () {
    game.enemy.image = enemy;
    load();
  });

  var background = game.processing.loadImage("../resources/background.png", null, function () {
    game.background = background;
    load();
  });

  var player = game.processing.loadImage("../resources/player.png", null, function () {
    game.player.image = player;
    load();
  });
}

function initializeEnemies () {
  var i = 0,
    offsetX = 0,
    offsetY = 0;

  game.enemies = [];

  for (i; i < game.enemyCount; i++) {
    if (offsetX >= game.width) {
      offsetY += game.enemy.height;
      offsetX = (parseInt(offsetX, 10) === game.width) ? 0 : game.enemy.width;
    }

    game.enemies.push({
      deathStage: 1,
      killed: false,
      pointX: offsetX,
      pointY: offsetY
    });

    offsetX += game.enemy.width * 2;
  }
}

function checkPlayerWin () {
  if (game.enemyKilled === game.enemyCount) {
    game.stage = 2;
  }
}

function checkEnemyHit () {
  var i = 0;
  var dot = game.dot;

  for (i; i < game.enemyCount; i++) {

    if (game.enemies[i].killed) {
      continue;
    }

    // A point (x, y) is inside a rectangle (x1,y1) - (x2, y2) if
    // (x1 <= x <= x2) and (y1 <= y <= y2)
    if ((game.enemies[i].pointX <= dot.pointX) && (dot.pointX <= game.enemies[i].pointX + game.enemy.width) &&
      (game.enemies[i].pointY <= dot.pointY) && (dot.pointY <= game.enemies[i].pointY + game.enemy.height)) {
      game.enemyKilled += 1;
      game.enemies[i].killed = true;
      return true;
    }
  }

  return false;
}

function moveBall () {
  var dot = game.dot;

  if (checkEnemyHit()) {
    dot.directionY *= -1;
    dot.directionX *= -1;
  }

  // Player Hit
  if ((dot.pointX > game.player.pointX && dot.pointX < game.player.pointX + game.player.width)
    && dot.pointY >= game.height - game.player.height) {
    dot.directionY *= -1;
    dot.speedX += Math.random() * (0.6 - 0.1) + 0.1;
    dot.speedY += Math.random() * (0.6 - 0.1) + 0.1;
  }

  if (dot.pointY >= game.height) {
    game.stage = -1;
  }

  if (dot.pointY < 0) {
    dot.directionY *= -1;
  }

  if (dot.pointX < 0 || dot.pointX >= game.width) {
    dot.directionX *= -1;
  }

  dot.pointX += (dot.speedX * dot.directionX);
  dot.pointY += (dot.speedY * dot.directionY);
}

function movePlayer (direction) {
  var offset = direction ? - 50 : 50;

  if (game.player.pointX + offset > -game.player.width/2 && game.player.pointX + offset < (game.width - game.player.width/2)) {
    game.player.pointX += offset;
  }
}

function drawBackground () {
  game.processing.background(73, 74, 125);
  game.processing.imageMode(game.processing.PConstants.CENTER);
  game.processing.image(game.background, game.centerX, game.centerY);
}

function drawBall () {
  var dot = game.dot;

  game.processing.ellipse(dot.pointX, dot.pointY, dot.size, dot.size);
  game.processing.fill.apply(game.processing, dot.color);
}

function drawPlayer () {
  var player = game.player;

  game.processing.imageMode(game.processing.PConstants.CORNER);
  game.processing.image(player.image, player.pointX, player.pointY, player.width, player.height);
}

function drawEnemies () {
  var enemy = game.enemy;
  var i = 0;

  for (i; i < game.enemyCount; i++) {
    if (game.enemies[i].killed) {
      if (game.enemies[i].deathStage < enemy.width) {
        game.enemies[i].deathStage += 1;
      } else {
        continue;
      }
    }

    game.processing.imageMode(game.processing.PConstants.CENTER);
    game.processing.image(game.enemy.image, game.enemies[i].pointX + (enemy.width/2), game.enemies[i].pointY + (enemy.height/2),
      enemy.width - game.enemies[i].deathStage, enemy.height - game.enemies[i].deathStage);
  }
}

function drawStartTheGame () {
  var text = game.text;

  game.processing.textSize(text.fontSize);
  game.processing.textAlign(game.processing.PConstants.CENTER,
    game.processing.PConstants.CENTER);

  game.processing.text("Press any button", 0,
    game.centerY - (text.boxHeight/2), game.width, text.boxHeight);
  game.processing.fill.apply(game.processing, text.color);
}

function drawPause () {
  var text = game.text;

  game.processing.textSize(text.fontSize);
  game.processing.textAlign(game.processing.PConstants.CENTER,
    game.processing.PConstants.CENTER);

  game.processing.text("Pause", 0,
    game.centerY - (text.boxHeight/2), game.width, text.boxHeight);
  game.processing.fill.apply(game.processing, text.color);
}

function drawGameOver () {
  var text = game.text;

  game.processing.textSize(text.fontSize);
  game.processing.textAlign(game.processing.PConstants.CENTER,
    game.processing.PConstants.CENTER);

  game.processing.text("Game Over. Reload page to continue",  0,
    game.centerY - (text.boxHeight/2), game.width, text.boxHeight);
  game.processing.fill.apply(game.processing, text.color);
}

function drawYouWin () {
  var text = game.text;

  game.processing.textSize(text.fontSize);
  game.processing.textAlign(game.processing.PConstants.CENTER,
    game.processing.PConstants.CENTER);

  game.processing.text("You Win",  0,
    game.centerY - (text.boxHeight/2), game.width, text.boxHeight);
  game.processing.fill.apply(game.processing, text.color);
}

function initialize(processing) {
  game.processing = processing;

  processing.keyPressed = keyPressed;
  processing.setup = setup;
  processing.draw = draw;
}

window.onload = function() {
  var container = document.getElementById("canvas_container");
  var canvas = document.getElementById("game_canvas");

  container.setAttribute("style", "width:" + window.innerHeight + "px");

  canvas.setAttribute('tabindex','0');
  canvas.focus();

  new Processing(canvas, initialize);
};