var game = {
  processing: null,
  stage: 1,
  centerX: 0,
  centerY: 0,
  width: 0,
  height: 0,
  enemyCount: 0,
  enemyKilled: 0,
  background: [210],
  enemy: {
    width: 0,
    height: 0,
    color: [0]
  },
  enemies: [

  ],
  text: {
    fontSize: 40,
    boxHeight: 250,
    color: [100, 0, 100]
  },
  dot: {
    pointX: 0,
    pointY: 0,
    directionX: -1,
    directionY: -1,
    speedX: 5,
    speedY: 5,
    size: 10,
    color: 0
  },
  player: {
    pointX: 0,
    pointY: 0,
    width: 150,
    height: 10,
    color: [0]
  }
};

function draw () {
  if (game.stage === 1) {
    drawBackground();
    moveBall();
    checkPlayerWin();
    drawBall();
    drawEnemies();
    drawPlayer();
  } else if (game.stage === 0) {
    drawPause();
  } else if (game.stage === -1) {
    drawBackground();
    drawGameOver();
  } else if (game.stage === 2) {
    drawBackground();
    drawYouWin();
  } else if (game.stage === - 2) {
    drawBackground();
    drawStartTheGame();
  }
}

function keyPressed () {
  if (game.stage === -2) {
    game.stage = 1;
  } else if (game.stage === -1){

  } else if (game.stage === 0) {
    if (game.processing.keyCode === 32) {
      game.stage = 1;
    }
  } else if (game.stage === 1) {
    if (game.processing.keyCode === 32) {
      game.stage = 0;
    }

    if (game.processing.keyCode === 39) {
      movePlayer(false);
    }

    if (game.processing.keyCode === 37) {
      movePlayer(true);
    }
  }

}

function setup() {
  game.height = game.width = window.innerHeight;

  game.processing.size(game.width, game.height);

  game.centerX = game.width / 2;
  game.centerY = game.height / 2;

  startTheGame();
}

function startTheGame () {
  game.stage = -2;

  game.enemyCount = 29;
  game.enemyKilled = 0;

  game.dot.speedX = Math.random() * (5 - 8) + 5;
  game.dot.speedY = Math.random() * (5 - 8) + 5;

  game.dot.pointX = 0;
  game.dot.pointY = game.centerY;

  game.player.pointX = game.centerX - (game.player.width/2);
  game.player.pointY = game.height - (game.player.height);

  game.enemy.width = game.width/19;
  game.enemy.height = game.width/19;

  initializeEnemies();
}

function initializeEnemies () {
  var i = 0,
    offsetX = 0,
    offsetY = 0;

  game.enemies = [];

  for (i; i < game.enemyCount; i++) {
    if (offsetX >= game.width) {
      offsetY += game.enemy.height;
      offsetX = (parseInt(offsetX, 10) === game.width) ? 0 : game.enemy.width;
    }

    game.enemies.push({
      pointX: offsetX,
      pointY: offsetY
    });

    offsetX += game.enemy.width * 2;
  }
}

function checkPlayerWin () {
  if (game.enemyKilled === game.enemyCount) {
    game.stage = 2;
  }
}

function checkEnemyHit () {
  var i = 0;
  var dot = game.dot;

  for (i; i < game.enemyCount; i++) {

    if (game.enemies[i] === null) {
      continue;
    }

    // A point (x, y) is inside a rectangle (x1,y1) - (x2, y2) if
    // (x1 <= x <= x2) and (y1 <= y <= y2)
    if ((game.enemies[i].pointX <= dot.pointX) && (dot.pointX <= game.enemies[i].pointX + game.enemy.width) &&
      (game.enemies[i].pointY <= dot.pointY) && (dot.pointY <= game.enemies[i].pointY + game.enemy.height)) {
      game.enemyKilled += 1;
      game.enemies[i] = null;
      return true;
    }
  }

  return false;
}

function moveBall () {
  var dot = game.dot;

  if (checkEnemyHit()) {
    dot.directionY *= -1;
    dot.directionX *= -1;
  }

  // Player Hit
  if ((dot.pointX > game.player.pointX && dot.pointX < game.player.pointX + game.player.width)
    && dot.pointY >= game.height - game.player.height) {
    dot.directionY *= -1;
    dot.speedX += Math.random() * (0.6 - 0.1) + 0.1;
    dot.speedY += Math.random() * (0.6 - 0.1) + 0.1;
  }

  if (dot.pointY >= game.height) {
    game.stage = -1;
  }

  if (dot.pointY < 0) {
    dot.directionY *= -1;
  }

  if (dot.pointX < 0 || dot.pointX >= game.width) {
    dot.directionX *= -1;
  }

  dot.pointX += (dot.speedX * dot.directionX);
  dot.pointY += (dot.speedY * dot.directionY);
}

function movePlayer (direction) {
  var offset = direction ? - 50 : 50;

  if (game.player.pointX + offset > -game.player.width/2 && game.player.pointX + offset < (game.width - game.player.width/2)) {
    game.player.pointX += offset;
  }
}

function drawBackground () {
  game.processing.background.apply(game.processing, game.background);
}

function drawBall () {
  var dot = game.dot;

  game.processing.ellipse(dot.pointX, dot.pointY, dot.size, dot.size);
  game.processing.fill(dot.color);
}

function drawPlayer () {
  var player = game.player;

  game.processing.rect(player.pointX, player.pointY, player.width, player.height);
  game.processing.fill.apply(game.processing, player.color);
}

function drawEnemies () {
  var enemy = game.enemy;
  var i = 0;

  for (i; i < game.enemyCount; i++) {
    if (game.enemies[i] === null) {
      continue;
    }

    game.processing.fill.apply(game.processing, enemy.color);

    game.processing.rect(game.enemies[i].pointX, game.enemies[i].pointY,
      enemy.width, enemy.height);
  }
}

function drawStartTheGame () {
  var text = game.text;

  game.processing.textSize(text.fontSize);
  game.processing.textAlign(game.processing.PConstants.CENTER,
    game.processing.PConstants.CENTER);

  game.processing.text("Press any button", 0,
    game.centerY - (text.boxHeight/2), game.width, text.boxHeight);
  game.processing.fill.apply(game.processing, text.color);
}

function drawPause () {
  var text = game.text;

  game.processing.textSize(text.fontSize);
  game.processing.textAlign(game.processing.PConstants.CENTER,
    game.processing.PConstants.CENTER);

  game.processing.text("Pause", 0,
    game.centerY - (text.boxHeight/2), game.width, text.boxHeight);
  game.processing.fill.apply(game.processing, text.color);
}

function drawGameOver () {
  var text = game.text;

  game.processing.textSize(text.fontSize);
  game.processing.textAlign(game.processing.PConstants.CENTER,
    game.processing.PConstants.CENTER);

  game.processing.text("Game Over. Reload page to continue",  0,
    game.centerY - (text.boxHeight/2), game.width, text.boxHeight);
  game.processing.fill.apply(game.processing, text.color);
}

function drawYouWin () {
  var text = game.text;

  game.processing.textSize(text.fontSize);
  game.processing.textAlign(game.processing.PConstants.CENTER,
    game.processing.PConstants.CENTER);

  game.processing.text("You Win",  0,
    game.centerY - (text.boxHeight/2), game.width, text.boxHeight);
  game.processing.fill.apply(game.processing, text.color);
}

function initialize(processing) {
  game.processing = processing;

  processing.keyPressed = keyPressed;
  processing.setup = setup;
  processing.draw = draw;
}

window.onload = function() {
  var canvas = document.getElementById("game_canvas");
  var container = document.getElementById("canvas_container");
  canvas.setAttribute('tabindex','0');
  canvas.focus();

  container.setAttribute("style","width:" + window.innerHeight + "px");
  new Processing(canvas, initialize);
};

