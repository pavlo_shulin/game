var clock = {
  processing: null,
  centerX: 0,
  centerY: 0,
  length: 0
};

function drawBackground () {
  clock.processing.background(224);
}

function drawDial () {
  clock.processing.fill(254);
  clock.processing.ellipse(clock.centerX, clock.centerY, clock.length + 10, clock.length + 10);
}

function drawArm(position, lengthScale, weight) {
  var x2 = clock.centerX + Math.sin(position * 2 * Math.PI) * lengthScale * clock.length/2;
  var y2 = clock.centerY - Math.cos(position * 2 * Math.PI) * lengthScale * clock.length/2;

  clock.processing.strokeWeight(weight);
  clock.processing.line(clock.centerX, clock.centerY, x2, y2);
}

function drawClockDot () {
  clock.processing.ellipse(clock.centerX, clock.centerY, 10, 10);
  clock.processing.fill(0, 0, 0);
}

function setup () {
  var processing = clock.processing;

  var width = window.innerHeight;
  var height = window.innerHeight;

  clock.centerX = width / 2;
  clock.centerY = height / 2;
  clock.length = Math.min(clock.centerX, clock.centerY);

  processing.size(width, height);
}

function draw () {
  var now = new Date();
  var hoursPosition = (now.getHours() % 12 + now.getMinutes() / 60) / 12;
  var minutesPosition = (now.getMinutes() + now.getSeconds() / 60) / 60;
  var secondsPosition = now.getSeconds() / 60;

  drawBackground();
  drawDial();

  drawArm(hoursPosition, 0.5, 5);
  drawArm(minutesPosition, 0.8, 3);
  drawArm(secondsPosition, 0.9, 1);

  drawClockDot();
}


function initialize(processing) {
  clock.processing = processing;

  processing.draw = draw;
  processing.setup = setup;
}


window.onload = function() {
  var canvas = document.getElementById("clock_canvas");
  var container = document.getElementById("canvas_container");
  canvas.setAttribute('tabindex','0');
  canvas.focus();

  container.setAttribute("style","width:" + window.innerHeight + "px");
  new Processing(canvas, initialize);
};